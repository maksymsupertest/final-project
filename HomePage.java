package pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.WebDriver;
import static org.openqa.selenium.By.xpath;

public class HomePage extends BasePage {

    @FindBy(xpath = "(//a[contains(text(), 'News')])[1]")
    private WebElement newsTab;
    @FindBy(xpath = "//button[@aria-label='Close']")
    private WebElement closeButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnNewsTab(){
        newsTab.click();
    }
    public void openHomePage(String url) {
        driver.get(url);
    }
    public void isCloseButtonVisible(){
        closeButton.isDisplayed();
    }
    public void clickOnCloseButton(){
        closeButton.click();
    }
}
