package pagefactory;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BBCNewsPage extends BasePage {
    @FindBy(xpath = "//div[contains(@class,'nw-c-top-stories__secondary-item')]//h3")
    private List<WebElement> secondaryArticleNames;

    @FindBy(xpath = "//div[@class='gs-u-display-none gs-u-display-block@m nw-o-news-wide-navigation']//a[@class='nw-o-link']/span[contains(text(), 'Coronavirus')]")
    private WebElement coronavirusTab;

    @FindBy(xpath = "(//h3[contains(text(),\"US 'lethal aid' sent to Ukraine amid Russia tensions\")])[1]")
    private WebElement topNewsArticle;

    @FindBy(xpath = "(//a[contains(@aria-label, 'From')])[1]")
    private WebElement categoryLinkHeadlineArticle;

    public BBCNewsPage(WebDriver driver) {
        super(driver);
    }

    public void checksSecondaryArticleNames() {
        String[] expectedNames = {"Novak Djokovic deported from Australia", "Texas synagogue hostage-taker was British", "Harry in legal fight to pay for police protection", "US and Canada hit by major winter storm", "Italian fashion great Nino Cerruti dies aged 91"};
        int counter = 0;
        for (WebElement element : secondaryArticleNames) {
            Assert.assertEquals(expectedNames[counter], element.getText());
            counter++;
        }



    }
    public WebElement getHeadlineArticle(){
        return topNewsArticle;

    }
    public String getTextFromCategoryLink(){
        return categoryLinkHeadlineArticle.getText();
    }
    public String getHeadlineArticleName(){
        return topNewsArticle.getText();
    }
}
