Feature:
  As a user
  I want to test all main site functionality
  So that i can be sure that site works correctly

  Scenario Outline: Check the name of the headline article
    Given User opens '<homePage>' page
    And User clicks on News
    And User checks close button visibility
    And User clicks on close button
    Then User checks the '<nameOfHeadlineArticle>'

    Examples:
      | homePage             | nameOfHeadlineArticle                                  |
      | https://www.bbc.com/ | US \'lethal aid\' sent to Ukraine amid Russia tensions |


  Scenario Outline: Check secondary article names
    Given User opens '<homePage>' page
    And User clicks on News
    And User checks secondary article names

    Examples:
      | homePage             |
      | https://www.bbc.com/ |

  Scenario Outline: Check the name of the first article in search results
    Given User opens '<homePage>' page
    And User clicks on News
    And User stores the text of the Category link of the headline article













