package stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pagefactory.BBCNewsPage;
import pagefactory.HomePage;

import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;
    WebDriver driver;
    HomePage homePage;
    BBCNewsPage bbcNewsPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @And("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }


    @And("User clicks on News")
    public void clickOnNews() {
        homePage.clickOnNewsTab();
        bbcNewsPage = pageFactoryManager.getBBCNewsPage();
    }
    @And("User checks close button visibility")
    public void closeButtonVisibility(){
        homePage.isCloseButtonVisible();

    }
    @And("User clicks on close button")
    public void clickOnCloseButton(){
        homePage.clickOnCloseButton();
    }


    @And("User checks secondary article names")
    public void checksNameOfSecondaryArticles(){
        bbcNewsPage.checksSecondaryArticleNames();
    }



    @Then("User checks the {string}")
    public void userChecksTheNameOfHeadlineArticle(final String expectedName) {

        bbcNewsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertEquals(bbcNewsPage.getHeadlineArticleName(), expectedName);
    }
}


