import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class Part1 {
    public void checkSecondaryArticles() {


    }

    public static void main(String[] args) {
        chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.bbc.com/");
        WebElement newsTab = driver.findElement(By.xpath("(//a[contains(text(), 'News')])[1]"));
        newsTab.click();
        WebElement topNewsText = driver.findElement(By.xpath("(//div[@data-entityid='container-top-stories#1']//h3)[1]"));
        Assert.assertEquals("Ash-covered Tonga is like a moonscape say residents", topNewsText.getText());
        driver.quit();


        driver.get("https://www.bbc.com/");
        WebElement newsTab2 = driver.findElement(By.xpath("(//a[contains(text(), 'News')])[1]"));
        newsTab2.click();
        List<WebElement> elementsList = driver.findElements(By.xpath("//div[contains(@class,'nw-c-top-stories__secondary-item')]//h3"));
        String[] expectedNames = {"Novak Djokovic deported from Australia", "Texas synagogue hostage-taker was British", "Harry in legal fight to pay for police protection", "US and Canada hit by major winter storm", "Italian fashion great Nino Cerruti dies aged 91"};
        int counter = 0;
        for (WebElement element : elementsList) {
            Assert.assertEquals(expectedNames[counter], element.getText());
            System.out.println(expectedNames[counter].equals(element.getText()));
            counter++;

        }


        driver.get("https://www.bbc.com/");
        WebElement newsTab3 = driver.findElement(By.xpath("(//a[contains(text(), 'News')])[1]"));
        newsTab3.click();
        WebElement categoryText = driver.findElement(By.xpath("(//a[contains(@aria-label, 'From')])[1]"));
        categoryText.getText();
        WebElement searchField = driver.findElement(By.xpath("//*[@id='orb-search-q']"));
        searchField.click();
        searchField.clear();
        searchField.sendKeys(categoryText.getText());
        searchField.submit();
        WebElement topArticle = driver.findElement(By.xpath("//span[contains(text(), 'At The Edge of Asia')]"));
        topArticle.getText();
        String s1 = "At The Edge of Asia";
        Assert.assertEquals(topArticle.getText(), s1);
        System.out.println(topArticle.getText().equals(s1));

        driver.get("https://www.bbc.com/");
        WebElement newsTab4 = driver.findElement(By.xpath("(//a[contains(text(), 'News')])[1]"));
        newsTab4.click();
        WebElement coronavirusTab = driver.findElement(By.xpath("//div[@class='gs-u-display-none gs-u-display-block@m nw-o-news-wide-navigation']//a[@class='nw-o-link']/span[contains(text(), 'Coronavirus')]"));
        coronavirusTab.click();
        WebElement contactBbcNewsTab = driver.findElement(By.xpath("//span[contains(text(), 'Contact BBC News')]"));
        contactBbcNewsTab.click();
        WebElement sendUsStoryTab = driver.findElement(By.xpath("//a[contains(text(), 'send us a story')]"));
        sendUsStoryTab.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement closeButton = driver.findElement(By.xpath("//button[@aria-label='Close']"));
        closeButton.click();
        WebElement storyField = driver.findElement(By.xpath("//textarea[@placeholder='Tell us your story. ']"));
        storyField.click();
        String s2 = "The story";
        storyField.sendKeys(s2);
        WebElement nameField = driver.findElement(By.xpath("//input[@placeholder='Name']"));
        nameField.click();
        WebElement checkBox = driver.findElement(By.xpath("//p[contains(text(), 'I accept the ')]//ancestor::div[@class='checkbox']//input"));
        checkBox.click();
        WebElement submitButton = driver.findElement(By.xpath("//button[contains(text(), 'Submit')]"));
        submitButton.click();
        WebElement errorMessage = driver.findElement(By.xpath("//div[text()=\"Name can't be blank\"]"));
        Assert.assertTrue(errorMessage.isDisplayed());


        driver.quit();


    }

}
